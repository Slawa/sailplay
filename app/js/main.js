$(function(){

    $('[data-footer-toggle]')
        .on('click', function(){
            $('body').toggleClass('show_full_footer');

            $("html, body").animate({ scrollTop: $(document).height() }, 400);
            return false;
        })

    $('.menu_toggle_icon').click( function(){
        if ($(window).width() >= 1201){
            var $menu = $('.l_menu'),
                $head = $('.s_mailing_head');

            if ($menu.hasClass('type_responsive')){
                $menu.toggleClass('show_full_menu');
                $head.toggleClass('show_full_menu');
            }
        }
    })


    // for common list
    $('[data-sendt-item-check]').on('change', function(){
        var $this = $(this),
            val = $this.prop('checked');

        if (val){
            $this.closest('.sender_item').addClass('show_state_checking');
            $('.s_mailing_toolbar').addClass('show_toolbar_checking');

            $('.toolbar_btn_all').addClass('state_check');
        } else {
            $this.closest('.sender_item').removeClass('show_state_checking');
            $('.s_mailing_toolbar').removeClass('show_toolbar_checking');

            $('.toolbar_btn_all').removeClass('state_check');
        }
    })

    $('.toolbar_btn_search').on('click', function(){
        $('.s_mailing_toolbar').addClass('show_toolbar_searching');
        $('.show_toolbar_searching').focus();
    })

    $('[data-show-tags-toggle]').on('change', function(){
        var $this = $(this);

        if ($this.prop('checked')){
            $this.closest('.toolbar_btn_tags').addClass('state_checking');
            $('.sender_item').addClass('show_state_tags');
        } else {
            $this.closest('.toolbar_btn_tags').addClass('state_checking');
            $('.sender_item').removeClass('show_state_tags');
        }
    });

    // fixed header
    $(window).scroll( function(){

        if ($(window).scrollTop() > 50){
            $('.s_mailing_head').addClass('state_small')
        } else {
            $('.s_mailing_head').removeClass('state_small');
        }
    })

    // for responsive
    $(window).resize( function(){
        var $menu = $('.l_menu'),
            $head = $('.s_mailing_head');

        if ($menu.hasClass('type_responsive')){
            if ($(this).width() <= 1201 ){
                $menu.removeClass('show_full_menu');
                $head.removeClass('show_full_menu');
            }
        }
    })

    // for archive page
    $('[data-archive-item-check]').on('change', function(){
        var $this = $(this),
            val = $this.prop('checked');

        if (val){
            $this.closest('.sender_item').addClass('show_state_checking');
            $('.s_mailing_toolbar').addClass('show_toolbar_creating');

            $('.toolbar_btn_all').addClass('state_check');
        } else {
            $this.closest('.sender_item').removeClass('show_state_checking');
            $('.s_mailing_toolbar').removeClass('show_toolbar_creating');

            $('.toolbar_btn_all').removeClass('state_check');
        }
    })


    //detail page


    var $select = $('#select_simple');
    $select.length && $select.selectize({
        create: true,
        sortField: 'text'
    });

    var $select2 = $('#user-link');
    $select2.length && $select2.selectize({
        create: true,
        sortField: 'text'
    });


    $('[data_tab]').on('click', function(e){
        var $this = $(this);

        if ($this.hasClass('selected')) return false;

        var $route = $($this.attr('href'));

        $route.siblings().hide().end()
              .show();

        $this.siblings().removeClass('selected').end()
              .addClass('selected');

        e.preventDefault();


        // хак для карты, чтобы перерисовать ее при переключении
        if ($this.attr('href') == '#geo_tab'){
            var $map = $('#map');

            if (!$map.children().length){
                $map.vectorMap({map: 'world_mill_en'})
            }
        }
    });

    var $graph = $('#graph');

    if ($graph.length) {
        $.jqplot('graph',  [[[1, 0.5],[2, 2.0],[3, 1.0]]], {
            axes: {
                xaxis: {
                    ticks: [0.9, 1, 2, 3, 4, 5]
                },
                yaxis: {
                    ticks: [0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0]
                }
            },
            grid: {
                drawGridLines: false,
                gridLineColor: '#f1f1ee',
                background: '#f1f1ee',
                borderColor: '#f1f1ee',
                borderWidth: 0,
                shadow: false
            },
            series: [
                {
                    color: '#6ace96',
                    lineWidth: 4,
                    shadow: false,
                    markerOptions: {
                        size: 10,            // size (diameter, edge length, etc.) of the marker.
                        color: '#48b943',    // color of marker, set to color of line by default.
                        shadow: false
                    }

                }
            ]
        })
    }


    $('[data-map-target]').hover(
        function () {
            var $this =  $(this),
                $target = $('#' + $this.data('map-target'));

            $('.mdetail_map_overlay')
                .find('.overlay_item').not($target).css('opacity', '0.1');

        },
        function () {
            $('.mdetail_map_overlay').find('.overlay_item').css('opacity', '1');
        })

    $('[data-tooltip]').hover(
        function(){
            var $this = $(this),
                $popup = $('<div class="tooltip_simple">' + $this.data('tooltip') + '</div>');

            $this.css('position', 'relative');

            $popup.appendTo($this).css({
                left: ($this.width() - $popup.width() - 20)/2,
                top: '-64px'
            });

            //by default - top position
            $popup.addClass('type_top').addClass('state_show');
        },
        function(){
            $('.tooltip_simple').remove();
        }
    )


})