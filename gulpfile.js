'use strict';


var gulp = require('gulp'),
	assemble = require('assemble'),
	app = assemble(),
	extname = require('gulp-extname'),
    browserSync = require('browser-sync').create(),
    pagespeed = require('psi'),
    reload = browserSync.reload,
    $ = require('gulp-load-plugins')();             // load plugins

var options = {
    data: 'app/layout/data/*.{json,yml}',
    layouts: 'app/layout/',
    partials: 'app/includes/*.hbs'
};


gulp.task('load', function(cb) {
  app.partials('app/partials/*.hbs');
  app.layouts('app/layout/*.hbs');
  app.pages('app/pages/*.hbs');
  cb();
});

gulp.task('assemble', ['load'], function() {
  return app.toStream('pages') //<= push "pages" collection into stream
    .pipe(app.renderFile()) //<= render pages with default engine (hbs)
    .pipe(extname())
    .pipe(app.dest('app/')); //<= write files to the "./site" directory
});

app.task('default', ['assemble']);

gulp.task('styles', function () {
  return gulp.src('app/styles/*.less')
      .pipe($.less())
      .on('error', console.error.bind(console))
      .pipe(gulp.dest('app/styles'))
      .pipe($.size());
});


gulp.task('styles', function () {
  return gulp.src('app/styles/*.less')
      .pipe($.less())
      .on('error', console.error.bind(console))
      .pipe(gulp.dest('app/styles'))
      .pipe($.size());
});

gulp.task('browserSync', function () {
  browserSync.init({
        server: {
            baseDir: "./app"
        }
    });
});


gulp.task('serve', ['styles', 'assemble', 'browserSync'], function () {
  gulp.watch('app/**/*.hbs', ['assemble']);
  gulp.watch('app/styles/**/*.{less, css}', ['styles', reload]);
  gulp.watch('app/js/**/*.js', reload);
  gulp.watch('app/img/**/*', reload);
  gulp.watch('app/*.html', reload);
});

gulp.task('pp', ['styles', 'assemble', 'browserSync'], function () {
    gulp.watch('app/styles/**/*.{less, css}', ['styles']);
    gulp.watch('app/**/*.hbs', ['assemble']);
});

gulp.task('default', function () {
  gulp.start('serve');
});

gulp.task('pagespeed', pagespeed.bind(null, {
  url: 'https://example.com',
  strategy: 'mobile'
}));


